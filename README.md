## VARIABLES D'ENVIRONNEMENT

Liste des variables d'environnement du Controller.

| VARIABLE D'ENV.               | DESCRIPTION                                            | 
|-------------------------------|--------------------------------------------------------|
| CONTRIB_CSV_OUTPUT_DIR         | Chemin ou nous allons écrire les Contributions en CSV  |
| CONTRIB_TRIG_OUTPUT_DIR        | Chemin ou nous allons écrire les Contributions en TRIG |
| RDFSTORE_USER                 | identifiant pour se connecter au triplestore           |  
| RDFSTORE_PWD                  | mot de passe pour se connecter au triplestore          | 
| RDFSTORE_ROOT_URI             | url du triplestore                                     |
| RDFSTORE_REPO_NAME            | nom du repo                                            |


Les répertoires doivent être présents sur le serveur.

### VERSION

"0.1.1-SNAPSHOT"