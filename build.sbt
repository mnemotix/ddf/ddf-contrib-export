import sbt.url
import sbt.Keys.{libraryDependencies, resolvers}

val meta = """META.INF(.)*""".r

ThisBuild / scalaVersion := Version.scalaVersion
/*ThisBuild / version := Version.synaptixVersion*/
ThisBuild / organization := "org.ddf"
ThisBuild / organizationName := "MNEMOTIX SCIC"

lazy val ddfContribExport = (project in file(".")).
  settings(
    name := "ddf-contrib-export",
    licenses := Seq("Apache 2.0" -> url("https://opensource.org/licenses/Apache-2.0")),
    developers := List(
      Developer(
        id = "prlherisson",
        name = "Pierre-René Lhérison",
        email = "pr.lherisson@mnemotix.com",
        url = url("http://www.mnemotix.com")
      ),
      Developer(
        id = "ndelaforge",
        name = "Nicolas Delaforge",
        email = "nicolas.delaforge@mnemotix.com",
        url = url("http://www.mnemotix.com")
      ),
    ),
    GitlabPlugin.autoImport.gitlabDomain :=  "gitlab.com",
    credentials += Credentials(Path.userHome / ".sbt" / ".credentials.gitlab"),
    publishTo := Some("gitlab maven" at "https://gitlab.com/api/v4/projects/28080370/packages/maven"),
    resolvers ++= Seq(
      Resolver.mavenLocal,
      Resolver.typesafeRepo("releases"),
      Resolver.typesafeIvyRepo("releases"),
      Resolver.sbtPluginRepo("releases"),
      "gitlab-maven" at "https://gitlab.com/api/v4/projects/21727073/packages/maven",
      "gitlab-maven-template-replacer" at "https://gitlab.com/api/v4/projects/35329974/packages/maven",
      "gitlab-maven-annotator" at "https://gitlab.com/api/v4/projects/16843866/packages/maven"
    ),
    libraryDependencies ++= Dependencies.core,
    assembly / test := {},
    assembly / assemblyMergeStrategy := {
      case PathList("javax", "servlet", xs@_*) => MergeStrategy.first
      case PathList(ps@_*) if ps.last endsWith ".html" => MergeStrategy.first
      //        case n if n.startsWith("reference.conf") => MergeStrategy.concat
      case n if n.endsWith(".conf") => MergeStrategy.concat
      case n if n.endsWith(".properties") => MergeStrategy.concat
      case PathList("META-INF", "services", xs@_*) => MergeStrategy.concat
      case PathList("META-INF", xs@_*) => MergeStrategy.discard
      case meta(_) => MergeStrategy.discard
      case x => MergeStrategy.first
    },
    Compile / mainClass := Some ("org.ddf.wiktionnaire.contrib.export.services.JobRunner"),
    run / mainClass := Some ("org.ddf.wiktionnaire.contrib.export.services.JobRunner"),

    assembly / mainClass := Some("org.ddf.wiktionnaire.contrib.export.services.JobRunner"),
    assembly / assemblyJarName := "ddf-contrib-export.jar",
    docker / imageNames  := Seq(
      ImageName(
        namespace = Some("registry.gitlab.com/mnemotix/ddf/ddf-contrib-export"),
        repository = name.value,
        tag = Some(version.value)
      )
    ),
    docker / buildOptions := BuildOptions(cache = false),
    docker / dockerfile := {
      val artifact: File = assembly.value
      val artifactTargetPath = s"/app/${artifact.name}"

      new Dockerfile {
        from("adoptopenjdk/openjdk11:alpine-slim")
        add(artifact, artifactTargetPath)
        entryPoint("java", "-jar", artifactTargetPath)
      }
    }
  ).enablePlugins(DockerPlugin)