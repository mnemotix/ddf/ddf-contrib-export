import sbt._

object Version {
  lazy val akkaActor = "2.5.23"
  lazy val alpakkaXml = "1.0.2"
  lazy val akkaHTTP = "10.1.9"
  lazy val config = "1.3.3"
  lazy val commons = "2.6"
  lazy val commonsCodec = "1.10"
  lazy val commonsHTTPClient = "3.1"
  lazy val htmlCleaner = "2.22"
  lazy val jenaLibs = "3.8.0"
  lazy val jgit = "5.6.1.202002131546-r"
  lazy val lifty = "0.1.2-SNAPSHOT"
  lazy val logback = "1.2.3"
  lazy val play = "2.7.0-RC9"
  lazy val playJson = "2.7.0-M1"
  lazy val playWSStandolone = "2.0.0-RC2"
  lazy val rdf4j = "2.5.3"
  lazy val rocksdb = "6.2.2"
  lazy val scalaLangMod = "1.1.1"
  lazy val scalaLogging = "3.9.0"
  lazy val scalaTest = "3.2.9"
  lazy val synaptixVersion = "3.1.2-SNAPSHOT"
  lazy val scalaVersion = "2.13.8"
  lazy val openCsv = "5.4"
}

//"com.softwaremill.sttp" %% "akka-http-backend" %
// lazy val amqpToolkit = "com.mnemotix" %% "synaptix-amqp-toolkit" % "0.1.6-SNAPSHOT"
// "org.eclipse.jgit" % "org.eclipse.jgit" % "5.6.1.202002131546-r"

object Library {
  lazy val config: ModuleID = "com.typesafe" % "config" % Version.config
  lazy val htmlCleaner = "net.sourceforge.htmlcleaner" % "htmlcleaner" % Version.htmlCleaner
  lazy val logbackClassic: ModuleID = "ch.qos.logback" % "logback-classic" % Version.logback
  lazy val rdf4j = "org.eclipse.rdf4j" % "rdf4j-runtime" % Version.rdf4j
  lazy val scalaTest: ModuleID = "org.scalatest" %% "scalatest" % Version.scalaTest
  lazy val synaptixAmqpToolKit = "com.mnemotix" %% "synaptix-amqp-toolkit" % Version.synaptixVersion
  lazy val synaptixRdfToolKit = "com.mnemotix" %% "synaptix-rdf-toolkit" % Version.synaptixVersion
  lazy val synaptixCore = "com.mnemotix" %% "synaptix-core" % Version.synaptixVersion
  lazy val synaptixCacheToolkit = "com.mnemotix" %% "synaptix-cache-toolkit" % Version.synaptixVersion
  lazy val quartz = "org.quartz-scheduler" % "quartz" % "2.3.1"
  lazy val openCsv = "com.opencsv" % "opencsv" % Version.openCsv
  lazy val ztZip = "org.zeroturnaround" % "zt-zip" % "1.14"
}

object Dependencies {

  import Library._

  val conf = List(config)
  val log = List(logbackClassic)
  val synaptix = List(synaptixCore, synaptixRdfToolKit, synaptixAmqpToolKit, synaptixCacheToolkit, openCsv, htmlCleaner, ztZip)
  val test = List(scalaTest)
  val jobQuartz = List(quartz)

  val compileScope = conf ++ synaptix ++ jobQuartz ++ log.map(_ % "compile")

  val testScope = (test ++ log).map(_ % "test")


  // Projects
  val core = compileScope ++ testScope
}
