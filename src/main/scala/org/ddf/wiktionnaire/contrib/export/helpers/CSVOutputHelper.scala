/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.wiktionnaire.contrib.`export`.helpers

import org.ddf.wiktionnaire.contrib.`export`.model.{LexicogEntry, VocsLexical}
import org.htmlcleaner.HtmlCleaner

object CSVOutputHelper {

  def toCsv(entries: Seq[LexicogEntry]): List[Array[String]] = {
    entries.map { entry =>
      Array(entry.statut.getOrElse(""), entry.wasDerivedFrom.getOrElse(""), entry.writtenRep,
        if (entry.lentries.cannonicalForm.isDefined) entry.lentries.cannonicalForm.get.otherForms.getOrElse("") else "",
        if (entry.lentries.cannonicalForm.isDefined) entry.lentries.cannonicalForm.get.phoneticRep.getOrElse("") else "",
        entry.lentries.pos.getOrElse(""),
        entry.lentries.multiWordType.getOrElse(""),
        entry.lentries.gender.getOrElse(""),
        entry.lentries.number.getOrElse(""),
        entry.lentries.transitivity.getOrElse(""),
        entry.lentries.term.getOrElse(""),
        entry.lentries.etymology.getOrElse(""),
        if (entry.lentries.sense.isDefined) entry.lentries.sense.get.uri.getOrElse("") else "",
        if (entry.lentries.sense.isDefined) entry.lentries.sense.get.url.getOrElse("") else "",
        if (entry.lentries.sense.isDefined) entry.lentries.sense.get.definition else "",
        if (entry.lentries.sense.isDefined) cleanHtml(entry.lentries.sense.get.definition) else "",
        if (entry.lentries.sense.isDefined) {
          if (entry.lentries.sense.get.usageExamples.isDefined) {
            entry.lentries.sense.get.usageExamples.get.exampleBibliographicalCitation.getOrElse("")
          }else ""
        }  else "",
        if (entry.lentries.sense.isDefined) {
          if (entry.lentries.sense.get.usageExamples.isDefined) {
            val cit = entry.lentries.sense.get.usageExamples.get.exampleBibliographicalCitation.getOrElse("")
            cleanHtml(cit)
          } else ""
        } else "",
        if (entry.lentries.sense.isDefined) entry.lentries.sense.get.hasLocalisation.getOrElse("") else "",
        if (entry.lentries.sense.isDefined) entry.lentries.sense.get.localisationGeonames.getOrElse("") else "",
       // ,"","","","","","",
        if (entry.lentries.sense.isDefined)  vocsLexical(entry.lentries.sense.get.vocsLexical,"domain") else "",
        if (entry.lentries.sense.isDefined)  vocsLexical(entry.lentries.sense.get.vocsLexical,"register") else "",
        if (entry.lentries.sense.isDefined)  vocsLexical(entry.lentries.sense.get.vocsLexical,"connotation") else "",
        if (entry.lentries.sense.isDefined)  vocsLexical(entry.lentries.sense.get.vocsLexical,"frequency") else "",
        if (entry.lentries.sense.isDefined)  vocsLexical(entry.lentries.sense.get.vocsLexical,"sociolect") else "",
        if (entry.lentries.sense.isDefined)  vocsLexical(entry.lentries.sense.get.vocsLexical,"temporality") else "",
        if (entry.lentries.sense.isDefined) entry.lentries.sense.get.scientificName.getOrElse("") else "",
        if (entry.lentries.sense.isDefined) entry.lentries.sense.get.aboutSens.getOrElse("") else "",
        if (entry.lentries.sense.isDefined)  vocsLexical(entry.lentries.sense.get.vocsLexical,"associativeRel") else "",
        if (entry.lentries.sense.isDefined)  vocsLexical(entry.lentries.sense.get.vocsLexical,"synonym") else ""
      )
    }.toList
  }

 def cleanHtml(text: String): String = {
   val cleaner = new HtmlCleaner()
   cleaner.clean(text).getText.toString
 }

 def vocsLexical(seqs :Seq[VocsLexical], voc: String) = {
   seqs.filter(_.vocab == voc).last.content.getOrElse("")
 }
}

//entries: Seq[Seq[LexicogEntry]]