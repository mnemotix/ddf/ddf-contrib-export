/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.wiktionnaire.contrib.`export`.helpers

import com.typesafe.config.{Config, ConfigFactory}

object ContribExportConfig {
  lazy val rdf: Config = Option(ConfigFactory.load().getConfig("rdf")).getOrElse(ConfigFactory.empty())
  lazy val repositoryName = rdf.getString("repository.name")

  lazy val contrib = Option(ConfigFactory.load().getConfig("contrib")).getOrElse(ConfigFactory.empty())
  lazy val bulkWrite = contrib.getInt("export.bulk.nbr")
  lazy val csvOutputDir = contrib.getString("export.output.contribs.csv.dir")
  lazy val trigOutputDir = contrib.getString("export.output.contribs.trig.dir")
  lazy val contribJobCronExpression = contrib.getString("export.cron.expression")
}
