/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.wiktionnaire.contrib.`export`.helpers

import org.ddf.wiktionnaire.contrib.`export`.model.{CanonicalForm, LexicalEntry, LexicogEntry, Sense, UsageExample, VocsLexical}
import org.eclipse.rdf4j.query.BindingSet

object RDFCSVMapper {
  def mapper(bs: BindingSet): LexicogEntry = {
    LexicogEntry(bidingGetter(bs, "statut"),
      None,
      bidingGetter(bs, "writtenRep").get,
      None,
      bidingNull(bs, "definition"),
      LexicalEntry(None, bidingGetter(bs,"aboutEtymology"), None, Some(Sense(bidingGetter(bs, "sense"),
        bidingGetter(bs, "senseURL"),
        bidingNull(bs, "definition"),
        Some(UsageExample(None, None, bidingGetter(bs, "exampleSource"))),
          Seq(
            VocsLexical("domain",bidingGetter(bs,"domain")), VocsLexical("register",bidingGetter(bs,"register")),
              VocsLexical("connotation",bidingGetter(bs,"connotation")), VocsLexical("frequency",bidingGetter(bs,"frequency")),
            VocsLexical("sociolect",bidingGetter(bs,"sociolect")), VocsLexical("temporality",bidingGetter(bs,"temporality")),
            VocsLexical("associativeRel",bidingGetter(bs,"associativeRel")), VocsLexical("synonym",bidingGetter(bs,"synonym"))
          ),
          bidingGetter(bs, "aboutSense"),bidingGetter(bs, "scientific"),bidingGetter(bs, "senseLocalisation"),bidingGetter(bs, "localisationGeonames") )),
        Some(CanonicalForm(bidingGetter(bs, "writtenRep").get, bidingGetter(bs, "phonetic"), bidingGetter(bs, "otherForms"))  ),
        bidingGetter(bs,"pos"), bidingGetter(bs,"multi"),
        bidingGetter(bs,"gender"), bidingGetter(bs, "number"), bidingGetter(bs, "transitivity"),bidingGetter(bs, "term"), None),
      bidingGetter(bs, "derived")
    )
  }

  def bidingGetter(bindingSet: BindingSet, variable: String) = {
    if (bindingSet.hasBinding(variable)) {
      Some(bindingSet.getBinding(variable).getValue.stringValue())
    }
    else None
  }

  def bidingNull(bindingSet: BindingSet, variable: String) = {
    if (bindingSet.hasBinding(variable)) {
      if (bindingSet.getBinding(variable).getValue.stringValue() != null) {
        bindingSet.getBinding(variable).getValue.stringValue()
      }
      else ""
    }
    else ""
  }
}