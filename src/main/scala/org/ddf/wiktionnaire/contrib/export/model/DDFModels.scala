/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.wiktionnaire.contrib.`export`.model

case class VocabularyEntity(voc: String, notation: String, concept: String, property: String, conceptLabel: String)

case class VocsLexical(vocab: String, content: Option[String])

case class UsageExample(example: Option[String], bibliographicalCitation: Option[String], exampleBibliographicalCitation: Option[String])

case class Sense(uri: Option[String], url: Option[String], definition: String, usageExamples: Option[UsageExample], vocsLexical: Seq[VocsLexical], aboutSens: Option[String], scientificName: Option[String], hasLocalisation: Option[String],
                 localisationGeonames: Option[String])

case class CanonicalForm(label: String, phoneticRep: Option[String],otherForms: Option[String])

case class SenseRelation(notationSkos: String, relationWord: Seq[String])

case class LexicalEntry(uri: Option[String], etymology: Option[String], lexicalEntryType: Option[String], sense: Option[Sense], cannonicalForm: Option[CanonicalForm],
                        pos: Option[String], multiWordType: Option[String], gender: Option[String], number: Option[String], transitivity: Option[String], term:Option[String], formHasLocalisation: Option[String])

case class LexicogEntry(statut: Option[String],uri: Option[String], writtenRep: String, aboutForm: Option[String], text: String, lentries: LexicalEntry, wasDerivedFrom: Option[String])