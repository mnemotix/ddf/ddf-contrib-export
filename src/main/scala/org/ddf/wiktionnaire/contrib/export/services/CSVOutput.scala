/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.wiktionnaire.contrib.`export`.services

import akka.NotUsed
import akka.stream.scaladsl.Flow
import com.opencsv.CSVWriter
import com.typesafe.scalalogging.LazyLogging
import org.zeroturnaround.zip.ZipUtil
import org.ddf.wiktionnaire.contrib.`export`.exceptions.ContribOutputException
import org.ddf.wiktionnaire.contrib.`export`.helpers.{CSVOutputHelper, ContribExportConfig}
import org.ddf.wiktionnaire.contrib.`export`.model.LexicogEntry

import java.io.{BufferedWriter, File, FileWriter}
import java.time.{LocalDateTime, ZoneId, ZoneOffset}
import java.time.format.DateTimeFormatter
import scala.collection.JavaConverters.asJavaIterableConverter
import scala.reflect.io.Directory


class CSVOutput extends LazyLogging {

  val format = "yyyyMMdd"
  val dtf = DateTimeFormatter.ofPattern(format)
  val now = LocalDateTime.now()
  val zone = ZoneId.of("Europe/Paris")

  val bulkWrite = ContribExportConfig.bulkWrite
  val dirName = ContribExportConfig.csvOutputDir

 // val file = new File(s"${dirName}output/").mkdirs()
 // val fileWriter = new FileWriter(new File(s"${dirName}output/${RandomNameGenerator.haiku}.csv"), true)

  lazy val fileWriter = new FileWriter(new File(s"${dirName}/${now.format(dtf)}/${now.toEpochSecond(ZoneOffset.of("+02:00")).toString}.csv"), true)

  lazy val outputFile = new BufferedWriter(fileWriter)
  lazy val csvWriter = new CSVWriter(outputFile)


  val csvSchema = Array("statut","derived","writtenRep","otherForms","phonetic","pos","multi","gender","number","transitivity","term",
    "aboutEtymology","sense","senseURL","definition","definitionSB","exampleWithSource","exampleWithSourceSB","senseLocalisation",
    "localisationGeonames","domain","register","connotation","frequency","sociolect","temporality",	"scientific","aboutSense",
    "associativeRel","synonym")

  def init(): Unit = {
    try {
      new File(s"${dirName}/${now.format(dtf)}").mkdirs()
    }
    catch {
      case t: Throwable =>
        throw ContribOutputException("An error occured during the creation of the directory for file output", Some(t))
    }
  }

  def writeAsync: Flow[Seq[LexicogEntry], Unit, NotUsed] = {
    try {
      csvWriter.writeNext(csvSchema)
      Flow[Seq[LexicogEntry]].map { entries =>
        val iterator = entries.grouped(bulkWrite)
        entries.grouped(bulkWrite).foreach { le =>
          csvWriter.writeAll(CSVOutputHelper.toCsv(le).asJava)
        }
      }
    }
    catch {
      case t: Throwable =>
        throw ContribOutputException("An error occured during the writing of the contrib", Some(t))
    }
  }

  def close(): Unit = {
    csvWriter.close()
    fileWriter.close()
    outputFile.close()
  }

  def zipCsv = {
    ZipUtil.pack(new File(s"${dirName}/${now.format(dtf)}/"), new File(s"${dirName}/${now.format(dtf)}.zip"))
  }

  def deleteDirectory = {
    val directory = new Directory(new File(s"${dirName}/${now.format(dtf)}/"))
    directory.deleteRecursively()
  }
}