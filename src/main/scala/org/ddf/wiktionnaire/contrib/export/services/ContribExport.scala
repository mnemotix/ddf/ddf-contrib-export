/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.wiktionnaire.contrib.`export`.services

import akka.Done
import akka.actor.ActorSystem
import akka.stream.scaladsl.{Sink, Source}

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContextExecutor, Future}

class ContribExport {

  implicit val system = ActorSystem(this.getClass.getSimpleName)
  implicit val executionContext: ExecutionContextExecutor = system.dispatcher

  lazy val csvOutput = new CSVOutput
  lazy val rdfExtractor = new RDFContribExportClient

  val sparql = s"""PREFIX owl: <http://www.w3.org/2002/07/owl#>
                  |PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
                  |PREFIX xml: <http://www.w3.org/XML/1998/namespace>
                  |PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
                  |PREFIX core: <http://www.w3.org/2004/02/skos/core#>
                  |PREFIX prov: <http://www.w3.org/ns/prov-o-20130430#>
                  |PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
                  |PREFIX sioc: <http://rdfs.org/sioc/ns#>
                  |PREFIX lemon: <http://lemon-model.net/lemon#>
                  |PREFIX lexicog: <http://www.w3.org/ns/lemon/lexicog#>
                  |PREFIX lexinfo: <http://www.lexinfo.net/ontology/2.0/lexinfo#>
                  |PREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#>
                  |PREFIX ddf: <http://data.dictionnairedesfrancophones.org/ontology/ddf#>
                  |PREFIX dinv: <http://data.dictionnairedesfrancophones.org/dict/inv/entry/>
                  |PREFIX dct: <http://purl.org/dc/terms/>
                  |PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
                  |PREFIX senserel:  <http://data.dictionnairedesfrancophones.org/authority/sense-relation/>
                  |PREFIX mnx: <http://ns.mnemotix.com/ontologies/2019/8/generic-model/>
                  |PREFIX onto: <http://www.ontotext.com/>
                  |
                  |# requête générique pour récupérer au format lisible le maximum d'infos sur toutes les entrées lexicales
                  |SELECT DISTINCT ?statut ?derived ?writtenRep ?otherForms ?phonetic ?pos ?multi ?gender ?number ?transitivity ?term
                  |?aboutEtymology
                  |?sense ?senseURL ?definition ?definitionSB ?examplesWithSource ?examplesWithSourceSB
                  |?senseLocalisation ?localisationGeonames
                  |?domain ?register ?connotation ?frequency ?sociolect ?temporality ?scientific
                  |?aboutSense
                  |?associativeRel ?synonyms
                  |
                  |#ontologies + voc
                  |FROM <http://www.ontotext.com/implicit> # necessary to include implicit, i.e infered statements
                  |FROM <http://data.dictionnairedesfrancophones.org/dict/ontologies/graph>
                  |FROM <http://data.dictionnairedesfrancophones.org/dict/voc/graph>
                  |FROM  <http://data.dictionnairedesfrancophones.org/dict/imported-ontologies/graph>
                  |# DDF contrib
                  |FROM  <http://data.dictionnairedesfrancophones.org/dict/contrib/graph>
                  |# Groups data (ACL)
                  |FROM  <http://data.dictionnairedesfrancophones.org/ontology/ddf#GroupsDataNG>
                  |
                  |WHERE {
                  |    ?entry ddf:hasLexicographicResource <http://data.dictionnairedesfrancophones.org/resource/ddf> .
                  |    optional{?entry <http://www.w3.org/ns/prov#wasDerivedFrom> ?derived .}
                  |    ?entry lexicog:describes ?lentry .
                  |    ?lentry ontolex:canonicalForm ?form .
                  |    ?form ontolex:writtenRep ?writtenRep .
                  |    #VALUES ?writtenRep {"roulodrome"@fr "canne"@fr "faire"@fr} .
                  |    optional { ?lentry ontolex:phoneticRep ?phonetic } # en attendant fix DIC-625
                  |    optional { ?form ontolex:phoneticRep ?phonetic } # à regrouper?
                  |    # Détails sur la LexicalEntry
                  |    optional { ?lentry ontolex:otherForm/ontolex:writtenRep ?otherForms . } # à regrouper?
                  |    optional { ?lentry ddf:hasPartOfSpeech/skos:prefLabel ?pos }.
                  |    optional { ?lentry ddf:multiWordType/skos:prefLabel ?multi } .
                  |    optional { {?lentry ddf:hasGender/skos:prefLabel ?gender} UNION
                  |    		   {?lentry ddf:inflectionHasGender/skos:prefLabel ?gender}}.
                  |    optional { {?lentry ddf:hasNumber/skos:prefLabel ?number } UNION
                  |    		   {?lentry ddf:inflectionHasNumber/skos:prefLabel ?number }} .
                  |    optional { ?lentry ddf:hasTransitivity/skos:prefLabel ?transitivity } .
                  |    optional { ?lentry lexinfo:termElement/skos:prefLabel ?term }.
                  |    # FIXME : change all nested queries
                  |    optional {
                  |       SELECT ?lentry (GROUP_CONCAT(?etym; SEPARATOR=", ") AS ?aboutEtymology)
                  |       WHERE{ ?lentry ddf:hasItemAboutEtymology/sioc:content ?etym } GROUP BY ?lentry }
                  |    # Détails sur le sense
                  |    ?lentry ontolex:sense ?sense .
                  |    BIND(IRI(CONCAT("https://www.dictionnairedesfrancophones.org/form/",STR(?writtenRep),"/sense/", ENCODE_FOR_URI(STRAFTER(STR(?sense), "/contrib/")))) AS ?senseURL) .
                  |    optional{ ?sense skos:definition ?definition . }
                  |    # check si supprimé
                  |    OPTIONAL{ ?sense mnx:hasDeletion ?deletion } .
                  |    BIND(IF(BOUND(?deletion), "supprimee", "non supprimee") AS ?statut) .
                  |	optional {
                  |        SELECT ?sense (GROUP_CONCAT(?exampleSource; SEPARATOR="|  ") AS ?examplesWithSource)
                  |        WHERE{
                  |            ?sense lexicog:usageExample ?ex .
                  |            ?ex rdf:value ?exValue .
                  |            OPTIONAL {?ex dct:bibliographicalCitation ?source } .
                  |            BIND(CONCAT(?exValue, "(source: ",?source,")") AS ?exampleSource)
                  |        	} GROUP BY ?sense}
                  |
                  |    optional {
                  |        SELECT ?sense (GROUP_CONCAT(?loc; SEPARATOR=", ") AS ?senseLocalisation)
                  |        WHERE{ ?sense ddf:hasLocalisation/skos:prefLabel ?loc } GROUP BY ?sense}
                  |    optional {
                  |        SELECT ?sense (GROUP_CONCAT(DISTINCT ?locGeo; SEPARATOR=", ") AS ?localisationGeonames)
                  |        WHERE{ ?sense ddf:hasLocalisation ?locGeo } GROUP BY ?sense}
                  |    optional{ ?sense ddf:hasDomain/skos:prefLabel ?domain } .
                  |    optional{ ?sense ddf:hasRegister/skos:prefLabel ?register } .
                  |    optional{ ?sense ddf:hasConnotation/skos:prefLabel ?connotation } .
                  |    optional{ ?sense ddf:hasFrequency/skos:prefLabel ?frequency } .
                  |    optional{ ?sense ddf:hasSociolect/skos:prefLabel ?sociolect } .
                  |    optional{ ?sense ddf:hasTemporality/skos:prefLabel ?temporality } .
                  |    optional{ ?sense ddf:hasTextualGenre/skos:prefLabel ?textual } .
                  |    optional{ ?sense ddf:hasScientificName ?scientific } .
                  |    optional {
                  |        SELECT ?sense (GROUP_CONCAT(?aboutS; SEPARATOR=", ") AS ?aboutSense)
                  |        WHERE{ ?sense ddf:hasItemAboutSense/sioc:content ?aboutS } GROUP BY ?sense}
                  |    ## Associative relation
                  |    optional {
                  |       SELECT DISTINCT ?sense (GROUP_CONCAT(DISTINCT ?relatedForm; SEPARATOR=", ") AS ?associativeRel)
                  |       WHERE{
                  |           {{?sense ontolex:isSenseOf/ddf:lexicalEntryHasSemanticRelationWith ?sr .}
                  |            UNION  {?sense ddf:senseHasSemanticRelationWith ?sr .}}
                  |           ?sr ddf:hasSemanticRelationType senserel:associativeRelation .
                  |           ?sr ddf:semanticRelationOfEntry/lexicog:describes/ontolex:canonicalForm/ontolex:writtenRep ?associativeForm }
                  |       GROUP BY ?sense}
                  |    ## Synonym relation
                  |    optional {
                  |           SELECT DISTINCT ?sense (GROUP_CONCAT(DISTINCT ?synonymForm; SEPARATOR=", ") AS ?synonyms)
                  |           WHERE{
                  |            {{?sense ontolex:isSenseOf/ddf:lexicalEntryHasSemanticRelationWith ?sr .}
                  |            UNION  {?sense ddf:senseHasSemanticRelationWith ?sr .}}
                  |               ?sr ddf:hasSemanticRelationType senserel:synonym .
                  |               ?sr ddf:semanticRelationOfEntry/lexicog:describes/ontolex:canonicalForm/ontolex:writtenRep ?synonymForm }
                  |       	GROUP BY ?sense}
                  |}
                  |""".stripMargin

  def init: Unit =  {
    rdfExtractor.init
    csvOutput.init()
  }

  def export: Future[Done] = {
    //val contributionsFut =  rdfExtractor.contribs(sparql)
    //val contributions = Await.result(contributionsFut, Duration.Inf)
    //contributions.foreach(println(_))

    Source.future(rdfExtractor.contribs(sparql)).via(csvOutput.writeAsync).runWith(Sink.ignore)
  }

  def close: Unit = {
    csvOutput.close()
  }

  def exportTrig: Unit = {
    rdfExtractor.getContribs
  }

  def afterAll: Boolean = {
    csvOutput.zipCsv
    Thread.sleep(2000)
    csvOutput.deleteDirectory
  }
}
