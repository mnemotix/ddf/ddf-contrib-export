/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.wiktionnaire.contrib.`export`.services

import org.ddf.wiktionnaire.contrib.`export`.helpers.ContribExportConfig
import org.ddf.wiktionnaire.contrib.`export`.services.job.ContribExportJob
import org.quartz.{CronScheduleBuilder, JobBuilder, TriggerBuilder}
import org.quartz.impl.StdSchedulerFactory

object JobRunner extends App {

  val contribExportTask = new ContribExportJob()
  contribExportTask.run()
  val quartz = StdSchedulerFactory.getDefaultScheduler
  val exportJob = JobBuilder.newJob(classOf[ContribExportJob]).withIdentity("ContribExportJob", "DDF").build()
  val trigger = TriggerBuilder.newTrigger().withIdentity("Trigger", "DDF").withSchedule(CronScheduleBuilder.cronSchedule(ContribExportConfig.contribJobCronExpression)).build()
  quartz.start()
  quartz.scheduleJob(exportJob, trigger)
}