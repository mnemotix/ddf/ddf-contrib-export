/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.wiktionnaire.contrib.`export`.services

import com.mnemotix.synaptix.rdf.client.RDFClient
import com.typesafe.scalalogging.LazyLogging
import org.ddf.wiktionnaire.contrib.`export`.helpers.{ContribExportConfig, RDFCSVMapper}
import org.ddf.wiktionnaire.contrib.`export`.model.LexicogEntry
import org.eclipse.rdf4j.model.Statement
import org.eclipse.rdf4j.model.impl.SimpleValueFactory
import org.eclipse.rdf4j.repository.RepositoryResult
import org.eclipse.rdf4j.rio.{RDFFormat, Rio}

import java.io.{File, FileOutputStream}
import java.time.{LocalDateTime, ZoneId, ZoneOffset}
import java.time.format.DateTimeFormatter
import scala.collection.mutable.ListBuffer
import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success}

class RDFContribExportClient(implicit val ec: ExecutionContext) extends LazyLogging {

  implicit lazy val conn = RDFClient.getReadConnection((ContribExportConfig.repositoryName))
  implicit lazy val writeConn = RDFClient.getWriteConnection((ContribExportConfig.repositoryName))

  lazy val format = "yyyyMMdd"
  lazy val dtf = DateTimeFormatter.ofPattern(format)
  lazy val now = LocalDateTime.now()
  lazy val zone = ZoneId.of("Europe/Paris")

  lazy val dirName = ContribExportConfig.trigOutputDir


  lazy val fileOutputStream = new FileOutputStream(s"${dirName}${now.format(dtf)}/${now.toEpochSecond(ZoneOffset.of("+02:00")).toString}.trig")

  def init: Boolean = {
    new File(s"${dirName}/${now.format(dtf)}").mkdirs()
    RDFClient.init()
  }

  def contribs(sparql: String): Future[Seq[LexicogEntry]] = {
    val lexicogEntries = ListBuffer[LexicogEntry]()
    val future = RDFClient.select(sparql).map { res =>
      val tupleQueryResult = res.resultSet
      while (tupleQueryResult.hasNext) {
        val bs = tupleQueryResult.next()
        lexicogEntries += RDFCSVMapper.mapper(bs)
      }
      lexicogEntries
    }
    future onComplete {
      case Success(i) => logger.info(s"we retreive ${i.size} contrib lexicog entry")
      case Failure(t) => logger.error("error during the contrib request", t)
    }
    future.map(_.toSeq)
  }

  def getContribs(): Unit = {
    val graph = SimpleValueFactory.getInstance().createIRI("http://data.dictionnairedesfrancophones.org/dict/contrib/graph")
    val statements: RepositoryResult[Statement] = conn.conn.getStatements(null, null, null, false, graph)
    Rio.write(statements, fileOutputStream, RDFFormat.TRIG)
  }
}