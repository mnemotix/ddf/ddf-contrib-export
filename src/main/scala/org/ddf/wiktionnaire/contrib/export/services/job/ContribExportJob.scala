/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.wiktionnaire.contrib.`export`.services.job

import com.typesafe.scalalogging.LazyLogging
import org.ddf.wiktionnaire.contrib.`export`.exceptions.ContribExporterException
import org.ddf.wiktionnaire.contrib.`export`.services.ContribExport
import org.quartz.{Job, JobExecutionContext}

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext}
import scala.util.{Failure, Success}

class ContribExportJob extends Job with LazyLogging {

  implicit val executionContext =  ExecutionContext.global

  override def execute(context: JobExecutionContext) = {
    run()
  }

  def run() = {
    logger.info(s"Exportation des contributions en cours")
    val contribExport = new ContribExport
    contribExport.init
    contribExport.exportTrig
    val done = contribExport.`export`
    done.onComplete {
      case Success(value) => logger.info(s"l'exportation des contributions est terminée avec la valeur ${value}")
      case Failure(e) => ContribExporterException("erreur durant l'exportation des contributions", Some(e))
    }
    Await.result(done, Duration.Inf)
    contribExport.close
    contribExport.afterAll
  }
}
