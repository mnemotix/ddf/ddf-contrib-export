/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.wiktionnaire.contrib.`export`.helpers

import org.scalatest.{FlatSpec, MustMatchers}

class CSVOutputHelperSpec extends FlatSpec with MustMatchers {

  it should "remove htlm tags" in {
    val s = "<a href=\"/form/créer\">Créer</a>, <a href=\"/form/produire\">produire</a>, <a href=\"/form/fabriquer\">fabriquer</a>, en parlant de toute <a href=\"/form/œuvre\">œuvre</a> <a href=\"/form/matériel\">matérielle</a>."
    println(CSVOutputHelper.cleanHtml(s))
  }

  it should "remove href and i" in {
    val s = "<i>Deuxième personne du singulier de l'impératif présent de</i> <a href=\"/form/canner\">canner</a>."
    println(CSVOutputHelper.cleanHtml(s))
  }

}
