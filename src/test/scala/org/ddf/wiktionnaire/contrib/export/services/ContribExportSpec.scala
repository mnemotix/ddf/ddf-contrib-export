/**
 * Copyright (C) 2013-2021 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.ddf.wiktionnaire.contrib.`export`.services

import org.ddf.wiktionnaire.contrib.`export`.ContribExportProjectSpec
import org.ddf.wiktionnaire.contrib.`export`.exceptions.ContribExporterException

import scala.concurrent.Await
import scala.concurrent.duration.Duration
import scala.util.{Failure, Success}

class ContribExportSpec extends ContribExportProjectSpec {
  it should "export contrib" in {
    val contribExport = new ContribExport
    val startTimeMillis = System.currentTimeMillis()
    contribExport.init
    contribExport.exportTrig
    val fut = contribExport.`export`
    fut.onComplete {
      case Success(value) => println(s"l'exportation des contributions est terminée avec la valeur ${value}")
      case Failure(e) => ContribExporterException("erreur durant l'exportation des contributions", Some(e))
    }
    val result = Await.result(fut, Duration.Inf)
    contribExport.close

    val endTimeMillis = System.currentTimeMillis()
    val durationSeconds = (endTimeMillis - startTimeMillis) / 1000
    println(s"${result} in $durationSeconds s")
  }

  it should "zip a dir" in {
    val csvOutput = new CSVOutput()
    val zipper = csvOutput.zipCsv
  }

  it should "delete a dir" in {
    val csvOuput = new CSVOutput()
    csvOuput.deleteDirectory
  }
}